
# Project 20-Diplomados

Forum koji studenti mogu da koriste za razmenjivanje iskustava i saveta.

## Developers

- [Marija Milićević, 1107/2019](https://gitlab.com/marija1023)
- [Natalija Jovanović, 1109/2019](https://gitlab.com/natalija_jovanovic)
- [Tijana Nikčević, 1103/2019](https://gitlab.com/tijana_nikcevic)
